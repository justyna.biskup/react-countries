import axios from "axios";

const ROOT_URL = "https://restcountries.eu/rest/v2";

export const FETCH_COUNTRIES = "FETCH_COUNTRIES";

export function fetchCountries(name) {
  const url = `${ROOT_URL}/name/${name}`;
  const request = axios.get(url);

  return {
    type: FETCH_COUNTRIES,
    payload: request,
  };
}
