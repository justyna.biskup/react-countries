import { combineReducers } from "redux";
import CountryReducer from "./reducer_countries";

const rootReducer = combineReducers({
  countries: CountryReducer
});

export default rootReducer;
