import React, { Component } from "react";
import { connect } from "react-redux";
import { fetchCountries } from "../actions/index";
import { bindActionCreators } from "redux";

class CountryList extends Component {
  renderCountry(countryData) {
    const name = countryData.name;
    const flag = countryData.flag;
    const nativeName = countryData.nativeName;
    const capital = countryData.capital;
    const currency = countryData.currencies[0].name;

    return (
      <tr key={name} >
        <td><img src={flag} className="flag" /> { name }</td>
        <td>{ nativeName }</td>
        <td>{ capital }</td>
        <td>{ currency }</td>
        <td><a className="btn btn-outline-primary btn-sm" target="_blank" href={`https://en.wikipedia.org/wiki/${nativeName}`}>wiki</a></td>
      </tr>
    );
  }

  render() {
    return (
      <table className="table table-hover">
        <thead>
          <tr>
            <th>Country</th>
            <th>Native name</th>
            <th>Capital</th>
            <th>Currency</th>
            <th>Wiki</th>
          </tr>
        </thead>
        <tbody>
          {this.props.countries.map(this.renderCountry)}
        </tbody>
      </table>
    );
  }
}

function mapStateToProps({countries}) {
  return { countries };
}

export default connect(mapStateToProps)(CountryList);
