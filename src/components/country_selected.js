import React, { Component } from 'react';

export const selectedCountriesList = ["Poland", "England", "Canada", "Norway"];

class CountrySelected extends Component {
  render() {
    return (
      <div className="country-selected">
        {
          selectedCountriesList.map(
            (country, index) =>
            <button className="btn btn-outline-primary" key={ index } > { country }</button>
          )
        }
      </div>
    )
  }
}

export default CountrySelected;
