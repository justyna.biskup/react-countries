import React, { Component } from "react";

import CountrySearch from "../containers/country_search";
import CountryShortList from "./country_shortlist";
import CountryList from "../containers/country_list";

export default class App extends Component {
  render() {
    return (
      <div>
        <div className="card">
          <CountrySearch />
          <CountryShortList />
        </div>
        <CountryList />
      </div>
    );
  }
}
